import pandas as pd


def get_titatic_dataframe() -> pd.DataFrame:
    df = pd.read_csv("train.csv")
    return df


def get_filled():
    df = get_titatic_dataframe()
    titles = ["Mr.", "Mrs.", "Miss."]

    df['title'] = [words[1].split()[0] for words in df['Name'].str.split(',')]

    missing = df[df['Age'].isna()].groupby('title').size()[titles]
    median = df.groupby('title')['Age'].median()[titles]

    result = [
        (titles[0], missing[0], int(median[0])), 
        (titles[1], missing[1], int(median[1])), 
        (titles[2], missing[2], int(median[2])),  
    ]

    return result
